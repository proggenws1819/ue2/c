/**
 * Diese Klasse berechnet das Volumen, die Oberfläche, die Mantelfläche
 * und die Mantellinie eines Kegelstumpfs.
 * Für den Radius der Grundfläche, den Radius der Deckfläche
 * und der Höhe werden die Werte, die über die Konsole in
 * dieser Reihenfolge übergeben werden, verwendet.
 *
 * @author Cedrico Knoesel
 * @since 05.12.18
 */
public class TruncatedCone {

    public static void main(String[] args) {
        //teste ob genug Argumente in der Kommandozeile übergeben wurden
        if (args.length < 3) {
            System.out.println("Wrong number of Arguments");
            return;
        }
        int baseRadius = Integer.parseInt(args[0]);
        int topRadius = Integer.parseInt(args[1]);
        int heigth = Integer.parseInt(args[2]);
        //teste ob unerlaubte Werte eingegeben wurden
        if (baseRadius < 0 || topRadius < 0 || heigth < 0 || baseRadius < topRadius) {
            System.out.println("Wrong Arguments");
            return;
        }
        double pi = Math.PI;
        //berechne die entsprechenden Werte
        double volume = ((heigth * pi) / 3)
                * (Math.pow(baseRadius, 2) + (baseRadius * topRadius) + Math.pow(topRadius, 2));
        double slantHeight = Math.sqrt(Math.pow(baseRadius - topRadius, 2) + Math.pow(heigth, 2));
        double surfaceArea = ((pi * Math.pow(baseRadius, 2))
                + (pi * Math.pow(topRadius, 2))
                + (((baseRadius + topRadius) * pi) * slantHeight));
        double lateralSurface = (baseRadius + topRadius) * pi * slantHeight;
        //gebe die Werte in der Kommandozeile aus
        System.out.printf("%f;%f;%f;%f", volume, surfaceArea, lateralSurface, slantHeight);
    }
}
